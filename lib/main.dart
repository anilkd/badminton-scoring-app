import 'package:badminton_app/badminton.dart';
import 'package:badminton_app/bloc/TournamentBloc.dart';
import 'package:badminton_app/bloc/bloc_provider.dart';
import 'package:badminton_app/pages/BadmintonMenuDrawer.dart';
import 'package:badminton_app/pages/HomePage.dart';
import 'package:flutter/material.dart';

void main() {


  runApp(new BadmintonApp());
}

class BadmintonApp extends StatelessWidget {
  static const kShrineAltDarkGrey = const Color(0xFF414149);
  static const kShrineAltYellow = const Color(0xFFFFCF44);
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Badminton App',
      theme: new ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or press Run > Flutter Hot Reload in IntelliJ). Notice that the
        // counter didn't reset back to zero; the application is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: BlocProvider<ApplicationBloc>(
        bloc: ApplicationBloc(),
        child: BadmintonMenuDrawer(),
      ),
    );
  }

}