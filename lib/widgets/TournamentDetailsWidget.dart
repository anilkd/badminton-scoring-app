import 'dart:async';

import 'package:badminton_app/bloc/TournamentBloc.dart';
import 'package:badminton_app/bloc/bloc_provider.dart';
import 'package:badminton_app/model/Tournament.dart';
import 'package:flutter/material.dart';

class TournamentDetailsWidget extends StatefulWidget {
  TournamentDetailsWidget({
    Key key,
    this.tournament,
    this.boxFit: BoxFit.cover,
  }) : super(key: key);

  final Tournament tournament;
  final BoxFit boxFit;

  @override
  TournamentDetailsWidgetState createState() => TournamentDetailsWidgetState();
}

class TournamentDetailsWidgetState extends State<TournamentDetailsWidget> {
  ApplicationBloc _bloc;

  ///
  /// In order to determine whether this particular Movie is
  /// part of the list of favorites, we need to inject the stream
  /// that gives us the list of all favorites to THIS instance
  /// of the BLoC
  ///
  StreamSubscription _subscription;

  @override
  void initState() {
    super.initState();
    _createBloc();
  }

  ///
  /// As Widgets can be changed by the framework at any time,
  /// we need to make sure that if this happens, we keep on
  /// listening to the stream that notifies us about favorites
  ///
  @override
  void didUpdateWidget(TournamentDetailsWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    _disposeBloc();
    _createBloc();
  }

  @override
  void dispose() {
    _disposeBloc();
    super.dispose();
  }

  void _createBloc() {
   // _bloc = ApplicationBloc();

    // Simple pipe from the stream that lists all the favorites into
    // the BLoC that processes THIS particular movie
  }

  void _disposeBloc() {
    //_subscription.cancel();
 //   _bloc.dispose();
  }

  @override
  Widget build(BuildContext context) {
//   context final ApplicationBloc bloc = BlocProvider.of<ApplicationBloc>(context);

    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          AspectRatio(
            aspectRatio: 1.0,
            child: Stack(
              fit: StackFit.expand,
              children: <Widget>[
                 Text( widget.tournament.name),

              ],
            ),
          ),
          SizedBox(height: 6.0),
          SizedBox(height: 4.0),
          Divider(),
          Container(
            padding: const EdgeInsets.fromLTRB(4.0, 4.0, 4.0, 8.0),
            child: Text(widget.tournament.stages.length.toString()),
          ),
        ],
      ),
    );
  }
}
