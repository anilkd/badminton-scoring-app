import 'package:badminton_app/model/Group.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class GroupWidget extends StatelessWidget {
  GroupWidget({ Key key, @required this.group, this.shape, this.onPressed})
      : assert(group != null),
        super(key: key);

  final Group group;
  final ShapeBorder shape;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    ShapeBorder _shape = shape != null ? shape : const RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(16.0),
        topRight: Radius.circular(16.0),
        bottomLeft: Radius.circular(16.0),
        bottomRight: Radius.circular(16.0),

      ),
    );
    final ThemeData theme = Theme.of(context);
    final TextStyle titleStyle = theme.textTheme.headline;
    final TextStyle descriptionStyle = theme.textTheme.subhead;

    return new InkWell(
      onTap: this.onPressed,
//      top: false,
//      bottom: false,

       child:  new Container(
      height: 20.0,
      width: 20.0,

       child: new Container(
//           padding: new EdgeInsets.only(top: 16.0),
       child:new Card(
           shape: _shape,
           child: new Container(
             margin: const EdgeInsets.symmetric(
               vertical: 8.0,
               horizontal: 8.0,
             ),
             child: new FittedBox(
               fit: BoxFit.scaleDown,
               alignment: Alignment.centerLeft,
               child: new Text(group.name,
                 style: titleStyle,
               ),
             ),
           ),
       )
     ),
       )
    );
  }
}

