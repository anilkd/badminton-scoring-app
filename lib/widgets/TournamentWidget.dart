import 'dart:async';

import 'package:badminton_app/bloc/TournamentBloc.dart';
import 'package:badminton_app/model/Tournament.dart';
import 'package:flutter/material.dart';

class TournamentWidget extends StatefulWidget {
  final Tournament tournament;
  final VoidCallback onPressed;

  TournamentWidget({
    Key key,
    @required this.tournament,
    @required this.onPressed,
  }) : super(key: key);

  @override
  TournamentWidgetState createState() => TournamentWidgetState();
}

class TournamentWidgetState extends State<TournamentWidget> {
  ApplicationBloc _bloc;

  ///
  /// In order to determine whether this particular Movie is
  /// part of the list of favorites, we need to inject the stream
  /// that gives us the list of all favorites to THIS instance
  /// of the BLoC
  ///
  StreamSubscription _subscription;

  @override
  void initState() {
    super.initState();
    _createBloc();
  }

  ///
  /// As Widgets can be changed by the framework at any time,
  /// we need to make sure that if this happens, we keep on
  /// listening to the stream that notifies us about favorites
  ///
  @override
  void didUpdateWidget(TournamentWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    _disposeBloc();
    _createBloc();
  }

  @override
  void dispose() {
    _disposeBloc();
    super.dispose();
  }

  void _createBloc() {

  }

  void _disposeBloc() {
  //  _subscription.cancel();
   // _bloc.dispose();
  }

  @override
  Widget build(BuildContext context) {
//    final FavoriteBloc bloc = BlocProvider.of<FavoriteBloc>(context);
    List<Widget> children = <Widget>[
      ClipRect(
        clipper: _SquareClipper(),
        child: Text(widget.tournament.name),
      ),
      Container(
        decoration: _buildGradientBackground(),
        padding: const EdgeInsets.only(
          bottom: 16.0,
          left: 16.0,
          right: 16.0,
        ),
        child: _buildTextualInfo(widget.tournament),
      ),
    ];

    //
    // If the movie is part of the favorites, put an icon to indicate it
    // A better way of doing this, would be to create a dedicated widget for this.
    // This would minimize the rebuild in case the icon would be toggled.
    // In this case, only the button would be rebuilt, not the whole movie card widget.
    //


    return InkWell(
      onTap: widget.onPressed,
      child: Card(
        child: Stack(
          fit: StackFit.expand,
          children: children,
        ),
      ),
    );
  }

  BoxDecoration _buildGradientBackground() {
    return const BoxDecoration(
      gradient: LinearGradient(
        begin: Alignment.bottomCenter,
        end: Alignment.topCenter,
        stops: <double>[0.0, 0.7, 0.7],
        colors: <Color>[
          Colors.black,
          Colors.transparent,
          Colors.transparent,
        ],
      ),
    );
  }

  Widget _buildTextualInfo(Tournament tournament) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Text(
          tournament.name,
          style: const TextStyle(
            fontWeight: FontWeight.w500,
            fontSize: 16.0,
            color: Colors.white,
          ),
        ),
        const SizedBox(height: 4.0),
        Text(
          tournament.stages.toString(),
          style: const TextStyle(
            fontSize: 12.0,
            color: Colors.white70,
          ),
        ),
      ],
    );
  }
}

class _SquareClipper extends CustomClipper<Rect> {
  @override
  Rect getClip(Size size) {
    return new Rect.fromLTWH(0.0, 0.0, size.width, size.width);
  }

  @override
  bool shouldReclip(CustomClipper<Rect> oldClipper) {
    return false;
  }
}


