import 'package:badminton_app/api/BadmintonApi.dart';
import 'package:badminton_app/bloc/TournamentBloc.dart';
import 'package:badminton_app/bloc/bloc_provider.dart';
import 'package:badminton_app/model/Tournament.dart';
import 'package:badminton_app/model/TournamentList.dart';
import 'package:badminton_app/pages/HomePage.dart';
import 'package:badminton_app/pages/Stages.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:io';

class BadmintonMenuDrawer extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return new BadmintonMenuDrawerState();
  }
}

class BadmintonMenuDrawerState extends State<BadmintonMenuDrawer> {
  int _selectedIndex = 0;
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  BadmintonApi api;

  @override
  void initState() {
    api=BadmintonApi();
    super.initState();
  }

  ///
  /// As Widgets can be changed by the framework at any time,
  /// we need to make sure that if this happens, we keep on
  /// listening to the stream that notifies us about favorites
  ///
  @override
  void didUpdateWidget(BadmintonMenuDrawer oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    super.dispose();
  }



  _getDrawerItemScreen(Tournament tournament) {
    return BlocProvider<ApplicationBloc>(
      bloc: ApplicationBloc(),
      child: new Stages(tournament),
    );
  }

  _onSelectItem(Tournament tournament, int index) {
    setState(() {
      _selectedIndex = index;
      _getDrawerItemScreen(tournament);
    });
    Navigator.of(context).pop(); // close the drawer
    Navigator.push(
      context,
      new MaterialPageRoute(
        builder: (context) => _getDrawerItemScreen(tournament),
      ),
    );
  }


  @override
  Widget build(BuildContext context) {

    return new Scaffold(
      key: _scaffoldKey,
      appBar: new AppBar(
        title: const Text('HTA Badminton'),
      ),

      drawer: new Drawer(
        child: new Column(
          children: <Widget>[
            new UserAccountsDrawerHeader(
                accountName: new Text(
                  "HTA Badminton",
                  style: new TextStyle(
                      fontSize: 18.0, fontWeight: FontWeight.w500),
                ),
                accountEmail: new Text(
                  "info@htauk.org",
                  style: new TextStyle(
                      fontSize: 18.0, fontWeight: FontWeight.w500),
                )),
            Flexible(child:FutureBuilder<TournamentList>(
                future: api.getTournaments(),
                builder: (BuildContext context,
                    AsyncSnapshot<TournamentList> snapshot) {
                  return ListView.builder(
                    itemCount:
                    (snapshot.data == null ? 0 : snapshot.data.tournaments.length),
                    itemBuilder: (context, index) {
                      return buildListTile(snapshot.data.tournaments, index);
                    },
                  );
                }))
          ],
        ),
      ),
      body: Text("MAIN"),
    )


    ;
  }

  ListTile buildListTile(List<Tournament> tournaments, int index) {
    return ListTile(title: Text(tournaments[index].name),
      selected: index == _selectedIndex,
      onTap: () => _onSelectItem(tournaments[index],index));
  }
}