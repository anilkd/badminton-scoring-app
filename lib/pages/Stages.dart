import 'package:badminton_app/model/Group.dart';
import 'package:badminton_app/model/Stage.dart';
import 'package:badminton_app/model/Tournament.dart';
import 'package:badminton_app/pages/TournamentDetailsPage.dart';
import 'package:badminton_app/widgets/Group.dart';
import 'package:flutter/material.dart';

class Stages extends StatefulWidget {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final Tournament tournament;

  Stages(this.tournament, {Key key}) : super(key: key);

  @override
  StagesState createState() => new StagesState(tournament);
}

class StagesState extends State<Stages> {
  // init the step to 0th position
  int current_step = 0;

  Tournament tournament;

  StagesState(this.tournament);

  @override
  Widget build(BuildContext context) {
    List<Step> my_steps = buildSteps(tournament.stages);
    return new Scaffold(
      // Appbar
      appBar: new AppBar(
        // Title
        title: new Text("Stages"),
      ),
      // Body
      body: new Container(
          child: new Stepper(
            // Using a variable here for handling the currentStep
        currentStep: this.current_step,
        // List the steps you would like to have
        steps: my_steps,
        // Define the type of Stepper style
        // StepperType.horizontal :  Horizontal Style
        // StepperType.vertical   :  Vertical Style
        type: StepperType.vertical,
        // Know the step that is tapped
        onStepTapped: (step) {
          // On hitting step itself, change the state and jump to that step
          setState(() {
            // update the variable handling the current step value
            // jump to the tapped step
            current_step = step;
          });
          // Log function call
          print("onStepTapped : " + step.toString());
        },
        onStepCancel: () {
          // On hitting cancel button, change the state
          setState(() {
            // update the variable handling the current step value
            // going back one step i.e subtracting 1, until its 0
            if (current_step > 0) {
              current_step = current_step - 1;
            } else {
              current_step = 0;
            }
          });
          // Log function call
          print("onStepCancel : " + current_step.toString());
        },
        // On hitting continue button, change the state
        onStepContinue: () {
          setState(() {
            // update the variable handling the current step value
            // going back one step i.e adding 1, until its the length of the step
            if (current_step < my_steps.length - 1) {
              current_step = current_step + 1;
            } else {
              current_step = 0;
            }
          });
          // Log function call
          print("onStepContinue : " + current_step.toString());
        },
      )),
    );
  }

  List<Step> buildSteps(List<Stage> stages) {
    List<Step> my_steps = [];
    stages.forEach((stage) => my_steps.add(new Step(
        title: new Text(stage.name),
        content: buildContent(stage),
        isActive: true)));
    return my_steps;
  }

  buildContent(Stage stage) {
    return Column(children: [Flexible (child: GridView.count(shrinkWrap:true,
        crossAxisCount: 3, children: buildGroupCards(context, stage.groups)), flex: 0,fit: FlexFit.tight,)]);
  }

  List<Widget> buildGroupCards(BuildContext context, List<Group> groups) {
    List<Widget> widgets = [];

    groups.forEach((group) => widgets.add(new GroupWidget(
          key: Key('Group_${group.name}'),
          onPressed: () {
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (BuildContext context) {
              return TournamentDetailsPage(
                data: tournament,
              );
            }));
          },
          group: group,
        )));
    return widgets;
  }

}
