import 'package:badminton_app/model/Tournament.dart';
import 'package:badminton_app/widgets/TournamentDetailsWidget.dart';
import 'package:flutter/material.dart';

class TournamentDetailsPage extends StatelessWidget {
  TournamentDetailsPage({
    Key key,
    this.data,
  }) : super(key: key);

  final Tournament data;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(data.name),
      ),
      body: TournamentDetailsWidget(
        tournament: data,
      ),
    );
  }
}
