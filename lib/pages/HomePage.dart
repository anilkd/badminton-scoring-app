

import 'package:badminton_app/bloc/TournamentBloc.dart';
import 'package:badminton_app/bloc/bloc_provider.dart';
import 'package:badminton_app/model/Tournament.dart';
import 'package:badminton_app/pages/TournamentDetailsPage.dart';
import 'package:badminton_app/widgets/TournamentWidget2.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget{

  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final Tournament tournament;

   HomePage(this.tournament, {Key key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    final ApplicationBloc applicationBloc = BlocProvider.of<ApplicationBloc>(context);

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Badminton'),

      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Expanded(
            // Display an infinite GridView with the list of all movies in the catalog,
            // that meet the filters
            child: StreamBuilder<List<Tournament>>(
                stream: applicationBloc.outTournaments,
                builder: (BuildContext context,
                    AsyncSnapshot<List<Tournament>> snapshot) {
                  return GridView.builder(
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      childAspectRatio: 1.0,
                    ),
                    itemBuilder: (BuildContext context, int index) {
                      return _buildTournamentCard(context, applicationBloc, index,
                          snapshot.data);
                    },
                    itemCount:
                    (snapshot.data == null ? 0 : snapshot.data.length),
                  );
                }),
          ),
        ],
      ),
    );
  }


  Widget _buildTournamentCard(BuildContext context, ApplicationBloc applicationBloc, int index, List<Tournament> tournaments) {
    // Get the MovieCard data
    final Tournament tournament =
    (tournaments != null && tournaments.length > index)
        ? tournaments[index]
        : null;

    if (tournament == null) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }

    return TournamentWidget(
        key: Key('tournament_${tournament.id}'),
        tournament: tournament,
        onPressed: () {
          Navigator
              .of(context)
              .push(MaterialPageRoute(builder: (BuildContext context) {
            return TournamentDetailsPage(
              data: tournament,
            );
          }));
        });
  }

}