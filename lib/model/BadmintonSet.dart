class BadmintonSet {
  final int teamOnePoints;
  final int teamTwoPoints;
  final String teamOneId;
  final String teamTwoId;

  BadmintonSet(
      this.teamOnePoints, this.teamTwoPoints, this.teamOneId, this.teamTwoId);

  BadmintonSet.fromJSON(Map<String, dynamic> json)
      : teamOnePoints = json["teamOnePoints"],
        teamTwoPoints = json["teamTwoPoints"],
        teamOneId = json["teamOneId"],
        teamTwoId = json["teamTwoId"];
}
