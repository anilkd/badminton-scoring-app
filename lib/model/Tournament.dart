import 'package:badminton_app/model/Stage.dart';
import 'package:badminton_app/model/Team.dart';

class Tournament {
  final String name;
  final String id;
  final int initialGroups;
  final List<Stage> stages;
  final List<Team> teams;

  Tournament(this.name, this.id, this.initialGroups, this.stages, this.teams);

  Tournament.fromJSON(Map<String, dynamic> json)
      : name = json["name"],
        id = json["id"],
        initialGroups = json["initialGroups"],
        teams = (json["teams"] as List<dynamic>)
            .map((item) => Team.fromJSON(item))
            .toList(),
        stages = (json["stages"] as List<dynamic>)
            .map((item) => Stage.fromJSON(item))
            .toList();
}
