import 'package:badminton_app/model/Player.dart';

class Team {
  final String name;
  final List<Player> players;
  final String description;

  Team(this.name, this.players, this.description);

  Team.fromJSON(Map<String, dynamic> json)
      : name = json["name"],
        players = (json["players"] as List<dynamic>)
            .map((item) => Player.fromJSON(item))
            .toList(),
        description = json["description"]{
    if(json !=null){

    }
  }
}
