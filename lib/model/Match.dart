import 'package:badminton_app/model/BadmintonSet.dart';
import 'package:badminton_app/model/Team.dart';

class BadmintonMatch{
  final String id;
  final String teamOne;
  final String teamTwo;
  final String matchDateTime;
  final int court;
  final int numberOfSets;
  final List<BadmintonSet> sets;

  BadmintonMatch(this.id, this.teamOne, this.teamTwo, this.matchDateTime, this.court, this.numberOfSets, this.sets);

  BadmintonMatch.fromJSON(Map<String, dynamic> json)
      : id = json["id"],
        teamOne = json["teamOne"],
        teamTwo = json["teamTwo"],
        matchDateTime = json["matchDateTime"],
        court = json["court"],
        numberOfSets = json["numberOfSets"],
        sets = (json["sets"] as List<dynamic>)
            .map((item) => BadmintonSet.fromJSON(item))
            .toList();
}