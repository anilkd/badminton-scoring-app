import 'package:badminton_app/model/Group.dart';

class Stage {
  final List<Group> groups;
  final String name;

  Stage(this.groups, this.name);

  Stage.fromJSON(Map<String, dynamic> json)
      : name = json["name"],
        groups = (json["groups"] as List<dynamic>)
            .map((item) => Group.fromJSON(item))
            .toList();
}
