class Player {
  final String name;
  final String phone;
  final String email;

  Player(this.name, this.phone, this.email);

  Player.fromJSON(Map<String, dynamic> json)
      : name = json["name"],
        phone = json["phone"],
        email = json["email"];
}
