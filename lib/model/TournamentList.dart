import 'package:badminton_app/model/Tournament.dart';

class TournamentList {
  List<Tournament> tournaments = <Tournament>[];

  TournamentList.fromJSON(List<dynamic> json)
      : tournaments = (json as List<dynamic>)
            .map((item) => Tournament.fromJSON(item))
            .toList();

  //
  // Return the Tournament by its id
  //
  Tournament findById(String id) =>
      tournaments.firstWhere((tournament) => tournament.id == id);
}
