import 'package:badminton_app/model/Match.dart';
import 'package:badminton_app/model/Team.dart';

class Group {
  final String name;
  final List<BadmintonMatch> matches;
  List<Team> toppers;

  Group(this.name,this.matches, this.toppers);

  Group.fromJSON(Map<String, dynamic> json) :

  name = json["name"],
  matches = (json["matches"] as List<dynamic>)
      .map((item) => BadmintonMatch.fromJSON(item))
      .toList();
  /*toppers = (json["toppers"] as List<dynamic>)
      .map((item) => Team.fromJSON(item))
      .toList();*/
}
