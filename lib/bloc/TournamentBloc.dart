import 'dart:async';

import 'package:badminton_app/api/BadmintonApi.dart';
import 'package:badminton_app/bloc/bloc_provider.dart';
import 'package:badminton_app/model/Tournament.dart';
import 'package:badminton_app/model/TournamentList.dart';
import 'package:collection/collection.dart';
import 'package:rxdart/rxdart.dart';

class ApplicationBloc implements BlocBase {
  ///
  /// Synchronous Stream to handle the provision of the tournaments
  ///

  PublishSubject<List<Tournament>> _tournamentsController = PublishSubject<List<Tournament>>();
  Sink<List<Tournament>> get _inTournaments => _tournamentsController.sink;
  Stream<List<Tournament>> get outTournaments => _tournamentsController.stream;


//  Stream<List<Tournament>> get outTournaments => _syncController.stream;

  ///
  StreamController<List<Tournament>> _cmdController =
      StreamController<List<Tournament>>.broadcast();

  StreamSink get sinkTournaments => _cmdController.sink;

  ApplicationBloc() {
    api.getTournaments().then((list) {
      tournaments = list;
      _tournamentsController.sink
          .add(UnmodifiableListView<Tournament>(tournaments.tournaments));
    });



  }

  void dispose() {
    _tournamentsController.close();
    _cmdController.close();
  }

  TournamentList tournaments;
}
