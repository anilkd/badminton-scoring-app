import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:badminton_app/model/Tournament.dart';
import 'package:badminton_app/model/TournamentList.dart';

class BadmintonApi {
  static const String baseUrl = 'localhost:8080';
  final httpClient = new HttpClient();

  Future<Tournament> getTournamentById({String id: "id"}) async {
    var uri = Uri.http(
      baseUrl,
      '/api/tournaments/$id',
      <String, String>{
        'key': 'value',
      },
    );

    var response = await _getRequest(uri);
    Tournament tournament = Tournament.fromJSON(json.decode(response));

    // Give some additional delay to simulate slow network
    await Future.delayed(const Duration(seconds: 1));

    return tournament;
  }

  Future<TournamentList> getTournaments() async {
    var uri = Uri.http(
      baseUrl,
      '/api/tournaments',
      <String, String>{
        'key': 'value',
      },
    );

    var response = await _getRequest(uri);
    TournamentList list = TournamentList.fromJSON(json.decode(response));

    return list;
  }

  Future<String> _getRequest(Uri uri) async {
    var request = await httpClient.getUrl(uri);
    var response = await request.close();

    return response.transform(utf8.decoder).join();
  }
}

BadmintonApi api = BadmintonApi();
